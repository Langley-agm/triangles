# TRIANGLES #

A small java application that based on three input numbers tells you what kind of Triangle it is.

### What's the purpose of this application? ###

The application serves as an exercise to demonstrate the value on Unit Testing.

### Exercise Steps: ###

1.- Get a group together and ask them to design a small app that will tell you what type of triangle it is based on the input of its three sides, they must start with a Triangle interface that will serve to the implementation team and the testing team.

2.- Divide the group in two teams, have one of them implement the class / classes, and the other design use cases to prove the other team's code doesn't work.

3.- Once the implementing team has finished, have them deliver their work to the testing team back and fort and see how many times it gets rejected.

4.- Teach them the JUnit test included in this example and see how much work, time and bugs they can save by just running them every time they think they are done.


Problem and design of the tests are based on the book: The Art of Software Testing by Glenford J. Myers, 3rd Edition.