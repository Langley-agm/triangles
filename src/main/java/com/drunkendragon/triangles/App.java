package com.drunkendragon.triangles;

import com.drunkendragon.triangles.entities.Triangle;

/**
 *
 * Triangles
 *
 * Small application that based on 3 input values that represent the sides of a triangle
 * will output what kind of Triangle is.
 *
 * The main objective of this application is to demonstrate how easy its is to miss errors
 * without using TDD in a small amount of time.
 *
 * @author Langley-agm
 *
 */
public final class App{

    public final static String INVALID_VALUES_ERROR_MSG = "You must specify three valid triangle sides, eg. App 3 2 5";

    public static void main( String[] args ){

        if(args == null || args.length != 3){

            System.out.println( INVALID_VALUES_ERROR_MSG );

        } else{

            float side1 = Float.valueOf(args[0]);
            float side2 = Float.valueOf(args[1]);
            float side3 = Float.valueOf(args[2]);

            try{
                System.out.println( new Triangle(side1,side2,side3).getType().name() );
            }catch(Exception e){ System.out.println(e.getMessage()); }

        }
    }
}