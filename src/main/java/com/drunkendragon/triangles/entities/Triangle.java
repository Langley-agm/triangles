package com.drunkendragon.triangles.entities;

import com.drunkendragon.triangles.entities.exceptions.NotATriangleException;
import static com.drunkendragon.triangles.entities.exceptions.NotATriangleException.ALL_SIDES_MUST_BE_POSITIVE_EXCEPTION_MSG;
import static com.drunkendragon.triangles.entities.exceptions.NotATriangleException.TRIANGLE_INEQUALITY_THEOREM_EXCEPTION_MSG;

/**
 * Basic Triangle object representation with 3 sides and a triangle type ( Equilateral, Isosceles, Scalene ).
 *
 * @author Langley-agm
 */
public final class Triangle {

    public static enum Type{ EQUILATERAL, ISOSCELES, SCALENE }

    private final float side1;
    private final float side2;
    private final float side3;
    private final Type type;

    public Triangle( float side1, float side2, float side3 ) throws NotATriangleException {

        if( side1 <= 0 || side2 <= 0 || side3 <= 0 ) throw new NotATriangleException( ALL_SIDES_MUST_BE_POSITIVE_EXCEPTION_MSG );

        if( side1 >= (side2 + side3) || side2 >= (side1 + side3) || side3 >= (side1 + side2) )
            throw new NotATriangleException(TRIANGLE_INEQUALITY_THEOREM_EXCEPTION_MSG);

        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;

        if ( side1 == side2 && side2 == side3  ) this.type = Type.EQUILATERAL;
        else if( side1 == side2 || side1 == side3 || side2 == side3 ) this.type = Type.ISOSCELES;
        else this.type = Type.SCALENE;

    }

    public final boolean isScalene() throws NotATriangleException {
        return this.getType() == Type.SCALENE;
    }

    public final boolean isIsosceles() throws NotATriangleException {
        return this.getType() == Type.ISOSCELES;
    }

    public final boolean isEquilateral() throws NotATriangleException {
        return this.getType() == Type.EQUILATERAL;
    }

    public float getSide1(){
        return side1;
    }

    public float getSide2(){
        return side2;
    }

    public float getSide3(){
        return side3;
    }

    public Type getType(){
        return type;
    }

}