package com.drunkendragon.triangles.entities.exceptions;

/**
 * This exception should be thrown whenever an attempt to create a <code>Triangle</code> object with incorrect values occurs.
 *
 * @author Langley-agm
 */
public class NotATriangleException extends Exception {

    public static final String ALL_SIDES_MUST_BE_POSITIVE_EXCEPTION_MSG = "All sides must have a positive value.";
    public static final String TRIANGLE_INEQUALITY_THEOREM_EXCEPTION_MSG = "Any side of a triangle is always shorter than the sum of the other two sides.";

    public NotATriangleException(String ex){
        super(ex);
    }

}