package com.drunkendragon.triangles;

import java.io.*;
import org.junit.*;

import static com.drunkendragon.triangles.App.INVALID_VALUES_ERROR_MSG;
import static com.drunkendragon.triangles.entities.exceptions.NotATriangleException.*;
import static com.drunkendragon.triangles.entities.Triangle.Type.*;

public class AppTest{

    private static final String OUTPUT_EXPECTED_FROM_INVALID_NUMBER_OF_PARAMS = INVALID_VALUES_ERROR_MSG;
    private static final String OUTPUT_EXPECTED_FROM_INVALID_ZERO_VALUED_PARAMS = ALL_SIDES_MUST_BE_POSITIVE_EXCEPTION_MSG;
    private static final String OUTPUT_EXPECTED_FROM_INVALID_NEGATIVE_VALUED_PARAMS = ALL_SIDES_MUST_BE_POSITIVE_EXCEPTION_MSG;
    private static final String OUTPUT_EXPECTED_FROM_INVALID_POSITIVE_VALUED_PARAMS = TRIANGLE_INEQUALITY_THEOREM_EXCEPTION_MSG;
    private static final String OUTPUT_EXPECTED_FROM_VALID_SCALENE_PARAMS = SCALENE.name();
    private static final String OUTPUT_EXPECTED_FROM_VALID_ISOSCELES_PARAMS = ISOSCELES.name();
    private static final String OUTPUT_EXPECTED_FROM_VALID_EQUILATERAL_PARAMS = EQUILATERAL.name();

    private static final ByteArrayOutputStream OUTPUT_STREAM = new ByteArrayOutputStream();

    private PrintStream interceptedOutput = null;

    @Before
    public void setUpStreams() {
        interceptedOutput = System.out;
        System.setOut(new PrintStream(OUTPUT_STREAM));
    }

    @After
    public void cleanUpStreams() {
        System.setOut( interceptedOutput );
    }

    @Test()
    //@Ignore("just a fix for coverage")
    public void coverageFix_Ignore() throws Exception { new App(); }

    @Test
    public void runningAppWithNoValues() throws Exception {
        OUTPUT_STREAM.reset();
        App.main(null);
        assert OUTPUT_STREAM.toString().contains(OUTPUT_EXPECTED_FROM_INVALID_NUMBER_OF_PARAMS);
    }

    @Test
    public void runningAppWithOnlyOneParam()throws Exception{
        OUTPUT_STREAM.reset();
        App.main( new String[]{"2.5"} );
        assert OUTPUT_STREAM.toString().contains(OUTPUT_EXPECTED_FROM_INVALID_NUMBER_OF_PARAMS);
    }

    @Test
    public void runningAppWithMoreThanThreeParams()throws Exception{
        OUTPUT_STREAM.reset();
        App.main(new String[]{"2.5", "3.5", "4.5", "5.5"});
        assert OUTPUT_STREAM.toString().contains(OUTPUT_EXPECTED_FROM_INVALID_NUMBER_OF_PARAMS);
    }

    @Test
    public void runningAppWithValidScaleneValues()throws Exception{
        OUTPUT_STREAM.reset();
        App.main( new String[]{"2.5","3.5","4.5"} );
        assert OUTPUT_STREAM.toString().contains(OUTPUT_EXPECTED_FROM_VALID_SCALENE_PARAMS);
    }

    @Test
    public void runningAppWithValidIsoscelesValues()throws Exception{
        OUTPUT_STREAM.reset();
        App.main( new String[]{"1","2.5","2.5"} );
        assert OUTPUT_STREAM.toString().contains(OUTPUT_EXPECTED_FROM_VALID_ISOSCELES_PARAMS);
    }

    @Test
    public void runningAppWithValidEquilateralValues()throws Exception{
        OUTPUT_STREAM.reset();
        App.main( new String[]{"2.5","2.5","2.5"} );
        assert OUTPUT_STREAM.toString().contains(OUTPUT_EXPECTED_FROM_VALID_EQUILATERAL_PARAMS);
    }

    @Test
    public void runningAppWithInvalidPositiveValues_FirstParamLongest()throws Exception{
        OUTPUT_STREAM.reset();
        App.main( new String[]{"3","1","1"} );
        assert OUTPUT_STREAM.toString().contains( OUTPUT_EXPECTED_FROM_INVALID_POSITIVE_VALUED_PARAMS );
    }

    @Test
    public void runningAppWithInvalidPositiveValues_SecondParamLongest()throws Exception{
        OUTPUT_STREAM.reset();
        App.main( new String[]{"1","5","4"} );
        assert OUTPUT_STREAM.toString().contains( OUTPUT_EXPECTED_FROM_INVALID_POSITIVE_VALUED_PARAMS );
    }

    @Test
    public void runningAppWithInvalidPositiveValues_ThirdParamLongest()throws Exception{
        OUTPUT_STREAM.reset();
        App.main( new String[]{"1","2","3"} );
        assert OUTPUT_STREAM.toString().contains( OUTPUT_EXPECTED_FROM_INVALID_POSITIVE_VALUED_PARAMS );
    }

    @Test
    public void runningAppWithZeroValuedParams()throws Exception{
        OUTPUT_STREAM.reset();
        App.main( new String[]{"0","0","0"} );
        assert OUTPUT_STREAM.toString().contains( OUTPUT_EXPECTED_FROM_INVALID_ZERO_VALUED_PARAMS );
    }

    @Test
    public void runningAppWithNegativeValuedParams()throws Exception{
        OUTPUT_STREAM.reset();
        App.main( new String[]{"-1","-2","-3"} );
        assert OUTPUT_STREAM.toString().contains( OUTPUT_EXPECTED_FROM_INVALID_NEGATIVE_VALUED_PARAMS );
    }

}