package com.drunkendragon.triangles.entities;

import com.drunkendragon.triangles.entities.exceptions.NotATriangleException;
import org.junit.Test;

import static com.drunkendragon.triangles.entities.Triangle.Type.*;

public class TriangleTest {

    @Test
    public void validSidesValuesAssignment() throws Exception {

        float side1 = 2.5f;
        float side2 = 3.5f;
        float side3 = 4.5f;

        float sumParams = side1 + side2 + side3;

        Triangle triangle = new Triangle( side1, side2, side3 );

        float sumTriangleSides = triangle.getSide1() + triangle.getSide2() + triangle.getSide3();

        assert sumParams == sumTriangleSides;

    }

    @Test
    public void validScalene() throws Exception {
        Triangle scaleneTriangle = new Triangle( 2.5f, 3.5f, 4.5f );
        assert scaleneTriangle.getType() == SCALENE;
        assert scaleneTriangle.isScalene();
    }

    @Test
    public void validIsosceles() throws Exception {
        Triangle isoscelesTriangle = new Triangle( 1f, 2.5f, 2.5f );
        assert isoscelesTriangle.getType() == ISOSCELES;
        assert isoscelesTriangle.isIsosceles();
    }

    @Test
    public void validEquilateral() throws Exception {
        Triangle equilateralTriangle = new Triangle( 2.5f, 2.5f, 2.5f );
        assert equilateralTriangle.getType() == EQUILATERAL;
        assert equilateralTriangle.isEquilateral();
    }

    @Test(expected = NotATriangleException.class)
    public void invalidPositiveValues_FirstSideLongest() throws Exception {
        new Triangle( 3, 1, 1 );
    }

    @Test(expected = NotATriangleException.class)
    public void invalidPositiveValues_SecondSideLongest() throws Exception {
        new Triangle( 1, 5, 4 );
    }

    @Test(expected = NotATriangleException.class)
    public void invalidPositiveValues_ThirdSideLongest() throws Exception {
        new Triangle( 1, 2, 3 );
    }

    @Test(expected = NotATriangleException.class)
    public void invalidZeroValuedParams() throws Exception {
        new Triangle( 0, 0, 0 );
    }

    @Test(expected = NotATriangleException.class)
    public void invalidNegativeValuedParams() throws Exception {
        new Triangle( -1, -2, -3 );
    }

}